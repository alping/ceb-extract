import os
import sqlite3
import contextlib
import json
from pathlib import Path
from typing import Union


@contextlib.contextmanager
def DatabaseResource(database_file: Union[Path, str]):
    class Database:
        def __init__(self, database_file: Union[Path, str]) -> None:
            try:
                self.connection = sqlite3.connect(database_file)
            except Exception as e:
                print(f"Could not create database: '{e}'")

        def get_conversations(self):
            cursor = self.connection.cursor()

            cursor.execute("SELECT uuid, name FROM conversations")
            conversations = {c[0]: c[1] for c in cursor.fetchall()}

            cursor.close()
            return conversations

        def get_messages_in_conversation(self, conversation_uuid):
            cursor = self.connection.cursor()

            cursor.execute(
                f"SELECT uuid, timeSent, body, status, type, relativeFilePath FROM messages WHERE conversationUuid == '{conversation_uuid}'"
            )
            messages = cursor.fetchall()

            cursor.close()
            return messages

        def cleanup(self):
            self.connection.close()

    database = Database(database_file)

    try:
        yield database
    finally:
        database.cleanup()


def export_json(database_file: Union[Path, str], json_dir: Union[Path, str]):
    database_file = Path(database_file)
    json_dir = Path(json_dir)

    with DatabaseResource(database_file) as database:
        conversations = database.get_conversations()

        messages = [
            {
                "uuid": c_id,
                "name": c_name,
                "messages": [
                    {
                        "uuid": m[0],
                        "timeSent": m[1],
                        "body": m[2],
                        "status": m[3],
                        "type": m[4],
                        "relativeFilePath": m[5],
                    }
                    for m in database.get_messages_in_conversation(c_id)
                ],
            }
            for c_id, c_name in conversations.items()
        ]

        with open(json_dir / "conversations.json", "w", encoding="utf-8") as f:
            json.dump(conversations, f, ensure_ascii=False)

        with open(json_dir / "messages.json", "w", encoding="utf-8") as f:
            json.dump(messages, f, ensure_ascii=False)


if __name__ == "__main__":
    from dotenv import load_dotenv

    load_dotenv()

    DATABASE_FILE = Path(os.environ.get("DATABASE_FILE"))
    JSON_DIR = Path(os.environ.get("JSON_DIR"))

    export_json(DATABASE_FILE, JSON_DIR)
