# Python
import os
import struct
import zlib
import sqlite3
from pathlib import Path
from io import BufferedReader
from typing import Union

# Third-party
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives.ciphers.aead import AESGCM


class DataInputStream:
    # https://stackoverflow.com/questions/29526121/python-equivalent-of-java-datainputstream-readutf
    def __init__(self, stream: BufferedReader) -> None:
        self.stream = stream

    def read_int(self) -> int:
        return struct.unpack(">i", self.stream.read(4))[0]

    def read_long(self) -> bytes:
        return struct.unpack(">q", self.stream.read(8))[0]

    def read_utf(self) -> bytes:
        utf_length: int = struct.unpack(">H", self.stream.read(2))[0]
        return self.stream.read(utf_length)

    def read_fully(self, length: int) -> bytes:
        return self.stream.read(length)

    def read(self) -> bytes:
        return self.stream.read()


class CEB:
    # Database Table Queries
    # https://github.com/iNPUTmice/ceb2txt/blob/386c53c2318711786bfe7bbb88af8d9c9a8a4fd5/src/main/java/im/conversations/ceb2txt/Main.java#L163
    _CREATE_ACCOUNTS_TABLE = "create table accounts (uuid text primary key, username text, server text, password text, display_name text, status number, status_message text, rosterversion text, options number, avatar text, keys text, hostname text, port number, resource text)"
    _CREATE_CONVERSATIONS_TABLE = "create table conversations (uuid text, accountUuid text, name text, contactUuid text, contactJid text, created number, status number, mode number, attributes text)"
    _CREATE_MESSAGES_TABLE = "create table messages (uuid text, conversationUuid text, timeSent number, counterpart text, trueCounterpart text, body text, encryption number, status number, type number, relativeFilePath text, serverMsgId text, axolotl_fingerprint text, carbon number, edited number, read number, oob number, errorMsg text, readByMarkers text, markable number, remoteMsgId text, deleted number, bodyLanguage text)"
    _CREATE_PREKEYS_TABLE = "create table prekeys (account text, id text, key text)"
    _CREATE_SIGNED_PREKEYS_TABLE = (
        "create table signed_prekeys (account text, id text, key text)"
    )
    _CREATE_SESSIONS_TABLE = (
        "create table sessions (account text, name text, device_id text, key text)"
    )
    _CREATE_IDENTITIES_TABLE = "create table identities (account text, name text, ownkey text, fingerprint text, certificate text, trust number, active number, last_activation number, key text)"

    def __init__(self, ceb_file: Union[Path, str]) -> None:
        try:
            with open(ceb_file, "rb") as f:
                stream = DataInputStream(f)

                self.version = stream.read_int()
                self.app = stream.read_utf()
                self.jid = stream.read_utf()
                self.timestamp = stream.read_long()
                self.iv = stream.read_fully(12)
                self.salt = stream.read_fully(16)

                self.data = stream.read()

        except Exception as e:
            print(f"Could not read CEB file: '{e}'")

    def __str__(self) -> str:
        return f"""\
            version: {self.version}
            app: {self.app}
            jid: {self.jid}
            timestamp: {self.timestamp}
            iv: {self.iv.hex()}
            salt: {self.salt.hex()}"""

    def decrypt_data(self, password: str) -> bytes:
        try:
            kdf = PBKDF2HMAC(
                algorithm=hashes.SHA1(),
                length=16,
                salt=self.salt,
                iterations=1024,
            )
            key = kdf.derive(bytes(password, "utf-8"))
            aesgcm = AESGCM(key)

            decrypted_data = aesgcm.decrypt(self.iv, self.data, None)
            decompressed_data = zlib.decompress(decrypted_data, 15 + 16)

            return decompressed_data

        except Exception as e:
            print(f"Could not decrypt the CEB file: '{e}'")


    def create_database(self, database: Union[Path, str], password: str) -> None:
        database_file = Path(database)
        if database_file.exists():
            raise Exception("Database already exists")

        sql_commands = self.decrypt_data(password).decode("utf-8")

        try:
            connection = sqlite3.connect(database_file)
            cursor = connection.cursor()

            # Create Tables
            cursor.execute(self._CREATE_ACCOUNTS_TABLE)
            cursor.execute(self._CREATE_CONVERSATIONS_TABLE)
            cursor.execute(self._CREATE_MESSAGES_TABLE)
            cursor.execute(self._CREATE_PREKEYS_TABLE)
            cursor.execute(self._CREATE_SIGNED_PREKEYS_TABLE)
            cursor.execute(self._CREATE_SESSIONS_TABLE)
            cursor.execute(self._CREATE_IDENTITIES_TABLE)

            # Execute all the SQL commands
            cursor.executescript(sql_commands)

            cursor.close()
            connection.close()

        except Exception as e:
            print(f"Could not create database: '{e}'")


if __name__ == "__main__":
    from dotenv import load_dotenv
    load_dotenv()

    BACKUP_FILE = os.environ.get("BACKUP_FILE")
    DATABASE_FILE = os.environ.get("DATABASE_FILE")
    PASSWORD = os.environ.get("PASSWORD")

    ceb = CEB(BACKUP_FILE)
    ceb.create_database(DATABASE_FILE, PASSWORD)
