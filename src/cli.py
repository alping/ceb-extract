import click
from src.extract import CEB
from src.query import export_json


@click.group()
def cli():
    pass


@click.command()
@click.argument("backup", type=click.Path(exists=True), required=True)
@click.argument("password", required=True)
def decrypt(backup, password):
    """Decrypts a CEB file and returns the contents

    BACKUP is the CEB file and PASSWORD is the password
    """
    ceb = CEB(backup)
    click.echo(ceb.decrypt_data(password))


@click.command()
@click.argument("backup", type=click.Path(exists=True), required=True)
@click.argument("database", type=click.Path(exists=False), required=True)
@click.argument("password", required=True)
def database(backup, database, password):
    """Creates a database from the CEB file

    BACKUP is the CEB file, DATABASE is the database file to be created, and PASSWORD is the password
    """
    ceb = CEB(backup)
    ceb.create_database(database, password)


@click.command()
@click.argument("database", type=click.Path(exists=True), required=True)
@click.argument("json", type=click.Path(exists=True), required=True)
def json(database, json):
    """Export JSON from the database

    DATABASE is the database file and JSON is the folder for the json files
    """
    export_json(database, json)


cli.add_command(decrypt)
cli.add_command(database)
cli.add_command(json)

if __name__ == "__main__":
    cli()
